/** Input class
(c) 2013 Caballa
Detects mouse and keyboard input by user.
Requires: canvas
*/
function Input() {
	this.mouseX = 0;
	this.mouseY = 0;
	this.worldX = 0;
	this.worldY = 0;
	this.key = new Array();
	this.mouseButton = 0;
	
	// Subscribers
	this.clickSubscribers = new Array();
	this.clickSubscribers[0] = new Array(); // lowest (render)
	this.clickSubscribers[1] = new Array(); // medium (UI panels/components)
	this.clickSubscribers[2] = new Array(); // highest (UI components)
	// TODO: Use object instead of arrays. E.g. clickSubscriber = {}, keys = array(). Each insert sort the keys. Each iterate lookup key array.
	
	this.clickPriority = new Array();
	this.keySubscribers = new Array();
	this.mouseDownSubscribers = new Array();
	this.mouseMoveSubscribers = new Array();
	
	// Local state
	this.isDragging = false;
	this.startX = 0;
	this.startY = 0;
	this.worldStartX = 0;
	this.worldStartY = 0;
}
Input.prototype.addClickSubscriber = function(subscriberFunction, priority) {
	if (priority == null) {
		priority = 0;
	}
	var newSubscriber = this.clickSubscribers[priority].length;
	this.clickSubscribers[priority][newSubscriber] = subscriberFunction;
	return newSubscriber;
}
Input.prototype.notifyClickSubscriber = function() {
	for (var i = this.clickSubscribers.length - 1; i >= 0; i--) {
		// layered priority system
		var clickQueue = this.clickSubscribers[i];
		for (var j = clickQueue.length - 1; j >= 0; j--) {
			// most recently added has higher priority
			if (clickQueue[j]() == true) {
				return;
			}
		}
	}
	
}
Input.prototype.addMouseDownSubscriber = function(subscriberFunction) {
	var newSubscriber = this.mouseDownSubscribers.length;
	this.mouseDownSubscribers[newSubscriber] = subscriberFunction;
	return newSubscriber;
}
Input.prototype.notifyMouseDownSubscriber = function() {
	for (var i = 0; i < this.mouseDownSubscribers.length; i++) {
		this.mouseDownSubscribers[i]();
	}
}
Input.prototype.addMouseMoveSubscriber = function(subscriberFunction) {
	var newSubscriber = this.mouseMoveSubscribers.length;
	this.mouseMoveSubscribers[newSubscriber] = subscriberFunction;
	return newSubscriber;
}
Input.prototype.notifyMouseMoveSubscriber = function() {
	for (var i = 0; i < this.mouseMoveSubscribers.length; i++) {
		this.mouseMoveSubscribers[i]();
	}
}
Input.prototype.addKeySubscriber = function(subscriberFunction) {
	var newSubscriber = this.keySubscribers.length;
	this.keySubscribers[newSubscriber] = subscriberFunction;
	return newSubscriber;
}
Input.prototype.notifyKeySubscriber = function() {
	for (var i = 0; i < this.keySubscribers.length; i++) {
		this.keySubscribers[i]();
	}
}
Input.prototype.removeSubscriber = function(list, id) {
	// swap to be-removed with the last one on the list
	list[id] = list[list.length-1];
	list.length--; // quick-delete
}
Input.prototype.handleKeyPress = function(key, isDown) {
	this.key[key] = isDown;
	this.notifyKeySubscriber();
}
Input.prototype.getKey = function(keyCode) {
	if (typeof(this.key[keyCode]) == "undefined") {
		this.key[keyCode] = false;
	}
	return this.key[keyCode];
}

var input = new Input();

function initInput(canvas) {
	if (canvas == null) {
		canvas = document.getElementById("cv");
	}
	if (canvas != null) {
		var mouseDown = function(e) {
			if (!input.isDragging) {
				input.startX = e.pageX - canvas.offsetLeft;
				input.startY = e.pageY - canvas.offsetTop;
				input.worldStartX = input.worldX;
				input.worldStartX = input.worldY;
			}
			input.mouseX = e.pageX - canvas.offsetLeft;
			input.mouseY = e.pageY - canvas.offsetTop;
			input.isDragging = true;
			input.mouseButton = e.button;
			input.notifyMouseDownSubscriber();
		}
		var mouseMove = function(e) {
			input.mouseX = e.pageX - canvas.offsetLeft;
			input.mouseY = e.pageY - canvas.offsetTop;
			if (input.isDragging) {
				input.worldX = input.worldStartX + input.mouseX - input.startX;
				input.worldY = input.worldStartX + input.mouseY - input.startY;
			}
			input.notifyMouseMoveSubscriber();
		}
		var mouseOut = function(e) {
			input.isDragging = false;
		}
		var mouseUp = function(e) {
			input.isDragging = false;
			input.notifyClickSubscriber();
		}
		var onContextMenu = function(e) {
			e.preventDefault();
			e.stopPropagation();
			return false;
		}
		var onSelectStart = function() {
			return false;
		}
		var keyDown = function(e) {
			input.handleKeyPress(e.keyCode, true);
		}
		var keyUp = function(e) {
			input.handleKeyPress(e.keyCode, false);
		}

		canvas.addEventListener("mousedown", mouseDown, true);
		canvas.addEventListener("mousemove", mouseMove, true);
		canvas.addEventListener("mouseup", mouseUp, true);
		canvas.addEventListener("keydown", keyDown, true); // tabindex must = 1 for canvas
		canvas.addEventListener("keyup", keyUp, true);
		canvas.onselectstart = onSelectStart;
		canvas.oncontextmenu = onContextMenu;
	}
}