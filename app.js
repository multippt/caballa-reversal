
var canvas;
var stage;

// Assets
var assets = {};
var minAssets = 1;
var loadedAssets = 0

// UI
var elements = {};

// Entry point for application
function Main() {
	canvas = $("#cv").get(0);
	stage = new createjs.Stage(canvas);
	stage.enableMouseOver(50);
	stage_loading();
	
	createjs.Ticker.setInterval(30);
	createjs.Ticker.setFPS(40);
	createjs.Ticker.addEventListener("tick", function() { stage.update(); });
	
	addAsset("bg", "bg.png");
}

function addAsset(name, file) {
	assets[name] = new Image();
	assets[name].name = name;
	assets[name].src = file;
	assets[name].onload = loadAsset;
}

function loadAsset(asset) {
	if (asset.target.name == "bg") {
		elements["bg"] = new createjs.Bitmap(assets["bg"]);
	}
	loadedAssets++;
	
	progress = Math.round(loadedAssets / minAssets * 100);
	
	if (loadedAssets == minAssets) {
		stage_game();
	}
}

// Preload loading stage
var progress = 0;
function stage_loading() {
	stage.removeAllChildren();
	// Fill
	var fill = new createjs.Shape();
	fill.graphics.beginFill("#000").drawRect(0, 0, canvas.width, canvas.height);
	stage.addChild(fill);
	
	// Load text
	var loadText = new createjs.Text("Loading: " + progress + "%", "Bold 12px Arial", "#EEE");
	loadText.x = canvas.width / 2;
	loadText.y = canvas.height / 2;
	stage.addChild(loadText);
	stage.update();
}

function stage_game() {
	stage.removeAllChildren();
	// Fill
	var fill = new createjs.Shape();
	fill.graphics.beginFill("#000").drawRect(0, 0, canvas.width, canvas.height);
	stage.addChild(fill);
	
	// Game object
	for (var i = 0; i < 10; i++) {
		for (var j = 0; j < 10; j++) {
			var gobj = new createjs.Shape();
			var color = "#aaa";
			if ((i + j) % 2 == 0) {
				color = "#bbb";
			}
			gobj.graphics.beginFill(color).drawRect(0, 0, 32, 32);
			gobj.x = i * 32;
			gobj.y = j * 32;
			gobj.value = {x: i, y: j, baseColor: color}; // game-values
			gobj.addEventListener("click", handleTile);
			gobj.addEventListener("mouseover", colorTileOver);
			gobj.addEventListener("mouseout", colorTileOut);
			gobj.addEventListener("tick", tickTile);
			elements["btn" + i + "_" + j] = gobj;
		}
	}
	
	// Insert elements
	for (var element in elements) {
		stage.addChild(elements[element]);
	}
	
	gameInit();
	
	stage.update();
}

// Start a game of reversi
function gameInit() {
	initGrid();
	setTile(4, 4, 1);
	setTile(5, 5, 1);
	setTile(4, 5, 2);
	setTile(5, 4, 2);
	gameState = "player";
	callPlayer();
}

var gridState = new Array();
var boardWidth = 10;
var boardHeight = 10;
function initGrid() {
	for (var i = 0; i < 10; i++) {
		gridState[i] = new Array();
		for (var j = 0; j < 10; j++) {
			gridState[i][j] = 0;
		}
	}
}

function updateTiles() {
	for (var i = 0; i < 10; i++) {
		for (var j = 0; j < 10; j++) {
			updateTile(i, j, gridState[i][j]);
		}
	}
}

function updateTile(x, y, owner) {
	var color = "#aaa";
	var valid = validTile(x, y);
	if (valid) {
		color = "#ccc";
	}
	if ((x + y) % 2 == 0) {
		color = "#bbb";
		if (valid) {
			color = "#ddd";
		}
	}
	if (owner == 1) {
		color = "#000";
	}
	if (owner == 2) {
		color = "#fff";
	}
	setColor(x, y, color);
}

function setTile(x, y, owner) {
	gridState[x][y] = owner;
	checkGridState(owner, x, y);
	updateTiles();
}

function setGridState(player, x, y) {
	gridState[x][y] = player;
}

function checkGridState(player, x, y) {
	setGridState(player, x, y);
	// check leftwards
	var n = -1;
	var s = -1;
	var e = -1;
	var w = -1;
	var ne = -1;
	var nw = -1;
	var se = -1;
	var sw = -1;
	for (var i = x-1; i >= 0; i--) {
		if (gridState[i][y] == 0) {
			break;
		}
		if (gridState[i][y] == player) {
			w = i;
			break;
		}
	}
	for (var i = x+1; i < boardWidth; i++) {
		if (gridState[i][y] == 0) {
			break;
		}
		if (gridState[i][y] == player) {
			e = i;
			break;
		}
	}
	for (var i = y-1; i >= 0; i--) {
		if (gridState[x][i] == 0) {
			break;
		}
		if (gridState[x][i] == player) {
			n = i;
			break;
		}
	}
	for (var i = y+1; i < boardHeight; i++) {
		if (gridState[x][i] == 0) {
			break;
		}
		if (gridState[x][i] == player) {
			s = i;
			break;
		}
	}
	// diagonal check
	var minnw = Math.min(x, y);
	for (var i = 1; i <= minnw; i++) {
		if (gridState[x-i][y-i] == 0) {
			break;
		}
		if (gridState[x-i][y-i] == player) {
			nw = i;
			break;
		}
	}
	var minne = Math.min(boardWidth-1-x, y);
	for (var i = 1; i <= minne; i++) {
		if (gridState[x+i][y-i] == 0) {
			break;
		}
		if (gridState[x+i][y-i] == player) {
			ne = i;
			break;
		}
	}
	var minsw = Math.min(x, boardHeight-1-y);
	for (var i = 1; i <= minsw; i++) {
		if (gridState[x-i][y+i] == 0) {
			break;
		}
		if (gridState[x-i][y+i] == player) {
			sw = i;
			break;
		}
	}
	var minse = Math.min(boardWidth-1-x, boardHeight-1-y);
	for (var i = 1; i <= minse; i++) {
		if (gridState[x+i][y+i] == 0) {
			break;
		}
		if (gridState[x+i][y+i] == player) {
			se = i;
			break;
		}
	}

	// flip in middle pieces
	if (n != -1) {
		for (var i = y-1; i >= n; i--) {
			setGridState(player, x, i);
		}
	}
	if (w != -1) {
		for (var i = x-1; i >= w; i--) {
			setGridState(player, i, y);
		}
	}
	if (s != -1) {
		for (var i = y+1; i < s; i++) {
			setGridState(player, x, i);
		}
	}
	if (e != -1) {
		for (var i = x+1; i < e; i++) {
			setGridState(player, i, y);
		}
	}
	if (nw != -1) {
		for (var i = 0; i < nw; i++) {
			setGridState(player, x-i, y-i);
		}
	}
	if (ne != -1) {
		for (var i = 0; i < ne; i++) {
			setGridState(player, x+i, y-i);
		}
	}
	if (sw != -1) {
		for (var i = 0; i < sw; i++) {
			setGridState(player, x-i, y+i);
		}
	}
	if (se != -1) {
		for (var i = 0; i < se; i++) {
			setGridState(player, x+i, y+i);
		}
	}
}

// Check win condition
function winCondition() {
	var player1 = countPlayer(1);
	var player2 = countPlayer(2);
	var tileCount = boardWidth * boardHeight;
	console.log(player1 + "," + player2);
	
	if (player1 == 0) {
		return 2;
	}
	if (player2 == 0) {
		return 1;
	}
	
	var total = player1 + player2;
	if (total == tileCount) {
		if (player1 < player2) {
			return 2;
		}
		if (player1 > player2) {
			return 1;
		}
		return 0;
	}
	return -1;
}

function checkWin() {
	var winState = winCondition();
	if (winState == 0) {
		gameState = "gameover";
		console.log("Result: Tie");
	}
	if (winState == 1) {
		gameState = "gameover";
		console.log("Result: Player1 wins");
	}
	if (winState == 2) {
		gameState = "gameover";
		console.log("Result: Player2 wins");
	}
}
function countPlayer(player) {
	var counter = 0;
	for (var i = 0; i < gridState.length; i++) {
		for (var j = 0; j < gridState[i].length; j++) {
			if (gridState[i][j] == player) {
				counter++;
			}
		}
	}
	return counter;
}


function setColor(x, y, color) {
	elements["btn" + x + "_" + y].graphics.beginFill(color).drawRect(0, 0, 32, 32);
	elements["btn" + x + "_" + y].value.baseColor = color;
}

function ownerColor(owner) {
	switch(owner) {
		case 0:
			return "";
		case 1:
			return "#000";
		case 2:
			return "#fff";
	}
}

function validTile(x, y) {
	var center = getOwner(x, y);
	if (center != 0) {
		return false;
	} else {
		var sum = 0;
		sum += getOwner(x-1, y-1);
		sum += getOwner(x, y-1);
		sum += getOwner(x+1, y-1);
		sum += getOwner(x-1, y);
		sum += getOwner(x+1, y);
		sum += getOwner(x-1, y+1);
		sum += getOwner(x, y+1);
		sum += getOwner(x+1, y+1);
		return (sum != 0);
	}
}

function getOwner(x, y) {
	if (x < 0 || y < 0 || x >= 10 || y >= 10) {
		return 0;
	}
	//return elements["btn" + x + "_" + y].value.owner;
	return gridState[x][y];
}

function inBounds(x, y) {
	if (x < 0 || y < 0 || x >= 10 || y >= 10) {
		return false;
	} else {
		return true;
	}
}

// Game logic
var gameState = "player";
function callPlayer2() {
	gameState = "otherplayer";
	console.log("It is other player turn");
}

function callPlayer() {
	gameState = "player";
	console.log("It is your turn");
}

var yourColor = 1;
var otherColor = 2;
function makeMove(x, y) {
	if (validTile(x, y)) {
		setTile(x, y, yourColor);
		callPlayer2();
	}
}

function handleInput(x, y) {
	if (gameState == "player") {
		makeMove(x, y);
		checkWin();
	} else if (gameState == "otherplayer") {
		//console.log("It is not your turn yet");
		makeMovePlayer2(x, y);
		checkWin();
	} else if (gameState == "gameover") {
		gameInit();
	}
}

// AI logic
function makeMovePlayer2(x, y) {
	if (validTile(x, y)) {
		setTile(x, y, otherColor);
		callPlayer();
	}
}

// Tile handling
function handleTile(e) {
	var obj = e.target;
	handleInput(obj.value.x, obj.value.y);
}
function colorTileOver(e) {
	var obj = e.target;
	var color = "#ff0";
	if (!validTile(obj.value.x, obj.value.y)) {
		color = "#f00";
	}
	obj.graphics.clear().beginFill(color).drawRect(0, 0, 32, 32);
}
function colorTileOut(e) {
	var obj = e.target;
	var color = obj.value.baseColor;
	obj.graphics.clear().beginFill(color).drawRect(0, 0, 32, 32);
}
function tickTile(e) {
	var obj = e.target;
/*
	if (input.key.length > 0) {
		console.log(input.key);
	}*/
	
	
	if (input.key[87]) {
		if (!obj.value.keyDown) {
			obj.value.keyDown = input.key[87];
			// clear graphics, so that it won't stack drawings and slow down render
			obj.graphics.clear().beginFill("#00f").drawRect(0, 0, 32, 32);
		}
	} else {
		if (obj.value.keyDown) {
			obj.value.keyDown = input.key[87];
			obj.graphics.clear().beginFill(obj.value.baseColor).drawRect(0, 0, 32, 32);
		}
	}
	/*
	if (input.key[87] == true) {
		obj.graphics.beginFill("#00f").drawRect(0, 0, 32, 32);
	} else {
		obj.graphics.beginFill(obj.value.baseColor).drawRect(0, 0, 32, 32);
	}
	*/
}