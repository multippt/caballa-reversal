function Game() {
	this.gridState;
	this.gridWidth = 10;
	this.gridHeight = 10;
	this.state;
	this.player = 1;
}
Game.prototype.init() {
	this.gridState = new Array();
	for (var i = 0; i < gridWidth; i++) {
		this.gridState[i] = new Array();
		for (var j = 0; j < gridHeight; j++) {
			this.gridState[i][j] = 0;
		}
	}
	
	this.gridState[4][4] = 1;
	this.gridState[5][5] = 1;
	this.gridState[4][5] = 2;
	this.gridState[5][4] = 2;
	this.state = "init";
}
Game.prototype.tick(game) {
	return function() {
		if (game.state == "init") {
			// pick a player to go first
			game.state = "player1";
			this.player = 1;
			return;
		}
	}
}
Game.prototype.set(player, x, y) {
	this.gridState[x][y] = player;
}